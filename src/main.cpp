/**************************************************************************/
/*!
    This example will attempt to connect to an ISO14443A
    card or tag and retrieve some basic information about it
    that can be used to determine what type of card it is.

    Note that you need the baud rate to be 115200 because we need to print
    out the data and read from the card at the same time!

    To enable debug message, define DEBUG in PN532/PN532_debug.h

*/
/**************************************************************************/

#include "Arduino.h"

#include <SPI.h>
#include <PN532_SPI.h>
#include "PN532.h"
#include "Wire.h"
#include "Keypad.h"

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include <avr/wdt.h>
#include "RS485NodeProto.h"

#include "pins.h"

// States...
#define PHASE_CARD 1
#define PHASE_PIN 2
#define PHASE_PENDING 3

int currentState = 1;

#define IDLETIME 14000

Adafruit_SSD1306 display(OLED_RESET);

// Var to store the last read uid in so we dont spam MQTT
uint8_t cardUid[] = { 0, 0, 0, 0, 0, 0, 0 };
uint8_t cardUidLen = 0;

const byte ROWS = 4; //four rows
const byte COLS = 3; //three columns

char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};

byte rowPins[ROWS] = {5, 4, 3, 2}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {8, 7, 6}; //connect to the column pinouts of the keypad

char keyBuffer[32];

// Track durations
unsigned long sentAt = 0;

unsigned long buzzerAt = 0;
int buzzerDuration = 500;

PN532_SPI pn532spi(SPI, SS);
PN532 nfc(pn532spi);

//#define RXNODE

RS485Controller rs485ctrl(Serial, TX_ENABLE_PIN, TX_ENABLE_PIN);

#ifdef RXNODE
RS485NodeProto  rs485proto(rs485ctrl, 2);
#else
RS485NodeProto  rs485proto(rs485ctrl, 1);
#endif


void buzzerSet(int duration = 600) {
  digitalWrite(BUZZER_PIN, HIGH);
  buzzerAt = millis();
  buzzerDuration = duration;
}

void buzzerTimeout() {
  if ((millis() - buzzerAt) > buzzerDuration) {
    digitalWrite(BUZZER_PIN, LOW);
  }
}

void Buzz(bool state) {
  if (state) {
    tone(BUZZER_PIN, 400);
  } else {
    noTone(BUZZER_PIN);
  }
}

void setIdle() {
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("Please swipe tag.");
  display.display();
}

void setup(void) {
  pinMode(TX_ENABLE_PIN, OUTPUT);
  digitalWrite(TX_ENABLE_PIN, HIGH);

  display.clearDisplay();
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)

  rs485proto.begin();

  pinMode(BUZZER_PIN, OUTPUT); //Set OC1B to output

  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("Initializing, please wait...");
  display.display();

  Serial.begin(9600);

  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    while (1); // halt
  }

  /* Got ok data, print it out!
  Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX);
  Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC);
  Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
  */

  // Set the max number of retry attempts to read from a card

  // This prevents us from waiting forever for a card, which is
  // the default behaviour of the PN532.
  nfc.setPassiveActivationRetries(0xFF);

  // configure board to read RFID tags
  nfc.SAMConfig();

  setIdle();
}

void getUidChars(uint8_t uid[], uint8_t uidLength, char *result, const unsigned result_length) {
  for (int i = 0; i < uidLength; i++) {
    sprintf(&result[2 * i], "%02x", uid[i]);
  }
}

void readRFID() {
  uint8_t success;

  // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  // 'uid' will be populated with the UID, and uidLength will indicate
  // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, cardUid, &cardUidLen);

  if (success) {
    /*
    Is it the same card that was read then we skip over the rest so we dont
     to verify it below
    */

    Buzz(true);
    delay(50);
    Buzz(false);

    if (cardUidLen == 4) {
      // We probably have a Mifare Classic card ...
      currentState = PHASE_PIN;
      display.clearDisplay();
      display.setTextColor(WHITE);
      display.setCursor(0,0);
      display.println("Please enter PIN.");
      display.display();
    }
  }
}

void processKeypad() {
  static unsigned long start_time = 0;
  char data[2];

  //' ' means no key press
  if (data[0] != ' ') {
    // 0 means the previous keypress hasn't been released
    if (data[0] != 0) {
      //clear the screen on the first press
      if (start_time == 0) {
        display.clearDisplay();
          display.print("Press # at end.");
        display.setCursor(0,1);
        display.display();
      }

      start_time = millis(); //mark the time of the key press
      //lcd.print(data);
      display.print(data);
      //display.print('*');     //don't print the passcode itself
      display.display();

      if (strlen(keyBuffer) < 10) {
        strcat(keyBuffer, data);
      }

      if (data[0] == '#') {
        //'#' means submit id code for access
        display.clearDisplay();
        display.setCursor(0,1);
        display.print("Checking...");
        display.display();

        // Set to PENDING so we dont do anything more.
        currentState = PHASE_PENDING;
        Buzz(false);
      }
    }
  } else {
    if (millis() - start_time > IDLETIME && start_time > 0) {
      start_time = 0;
      keyBuffer[0] = 0;
    }
  }
}

void processCtrl() {
  if (currentState == PHASE_PENDING && sentAt == 0) {
    char msg[100] = {};

    char id[10] = {};
    int idLen = 0;

    getUidChars(cardUid, cardUidLen, id, idLen);
    //Serial.println(id);

    sprintf(msg, "VERIFY;credential=%s,pin=%s", id, keyBuffer);

    int i = 0;
    while (i < 3) {
      rs485proto.sendPacket(PTYPE_SINGLE, 1, msg, strlen(msg), 3);
      i ++;
      delay(200);
    }

    sentAt = millis();
  }
}

void loop() {
  switch (currentState) {
    case PHASE_CARD:
      //Serial.println("Reading card...");
      readRFID();
      break;
    case PHASE_PIN:
      processKeypad();
      break;
  }

  processCtrl();
}
